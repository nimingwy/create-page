const fs = require("fs")
const fileSave = require("file-save");
const render = require("json-templater/string");
const path = require('path')
const {getSrcDir} = require("./utils")
// 文件是否存在
function hasFile(options){
    let viewPath = path.join(getSrcDir(), options.view + "/" + options.TemplateName);
    const exists = fs.existsSync(viewPath);
    if (exists) {
        console.log("页面已存在");
        process.exit(1);
    }

    fs.mkdir(viewPath, function (err) {
        if (err) {
            console.error(err);
            console.log("页面已存在");
            process.exit(1);
        } else {
            console.log("页面创建成功");
        }
    });
}

// 文件名称是否重复
function hasFileName({templateRouteList,pathName}){
    const hasName = templateRouteList.some((e) => e.name === pathName);
    if (hasName) {
        console.error("[页面名]重复 - 页面已存在,请更换其他名称");
        process.exit(1);
    }
}
// 模块是否存在
function hasModules(options){
    const {modulesJSON,pathName} = options
    options.pageName = modulesJSON[pathName];
    if (!options.pageName) {
        console.error("[模块]不存在 - 模块不存在,请更换其他名称");
        process.exit(1);
    }
    options.ageList = []
    if (!Array.isArray(options.pageName)) {
        options.pageName = `page/${options.pageName}`;
    } else {
        const copyModules = options.pageName;
        options.pageName = `page/${copyModules[0]}`;
        options.pageList = copyModules.splice(1);
    }
}
function setRouteInfo(options,routeOptions){
    const {TemplateName,view,pageName,viewDir} = options
    options.templateRouteList.push(
        Object.assign(routeOptions, {
            path: "/" + TemplateName,
            name: TemplateName,
            components: "./" + view + "/" + TemplateName + "/index.vue",
            meta:{
                title:TemplateName
            }
        })
    );
    const modulesTempaltepath = path.join(__dirname, pageName);
    options.pageTemplateContent = fs.readFileSync(modulesTempaltepath);

    const modulePath = viewDir + "/" + TemplateName + "/";
    options.DIR_OUTPUT_PATH_VUE = modulePath + "index.vue";
    options.DIR_OUTPUT_PATH_LESS = modulePath + "index.less";
}
// 保存Router模板
function saveRouterTemplate({templateRoutePath,templateRouteList}) {
    const TemplatePath = path.join(__dirname, templateRoutePath);
    fileSave(TemplatePath)
        .write(JSON.stringify(templateRouteList, null, "  "), "utf8")
        .end("\n");
    console.log("路由添加成功");
}
function saveFile({pageList = [],view,pageName, replaceTemplate = {}}) {
    pageList.forEach((e) => {
        const template = fs.readFileSync(path.join(__dirname, `page/${e}`));
        let _path = path.join(
            getSrcDir(),
            `${view}/${pageName}/${e.split("/").splice(1)}`
        );
        fileSave(_path)
            .write(render(template.toString(), replaceTemplate), "utf8")
            .end("\n");
    });
}
function saveVue(out_path, template, replace = {}) {
    replace["less"] = `import "./index.less"`;
    fileSave(out_path)
        .write(render(template.toString(), replace), "utf8")
        .end("\n");
}
function saveLess(out_path, template, replace = {}) {
    fileSave(out_path)
        .write(render(template.toString(), replace), "utf8")
        .end("\n");
}
module.exports = {
    hasFile,
    hasFileName,
    hasModules,
    setRouteInfo,
    saveRouterTemplate,
    saveFile,
    saveVue,
    saveLess
}
