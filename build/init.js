const path = require("path")
const {getSrcDir} = require("./utils")
// 模块验证
function modulesVerification() {
    if (!process.argv[2]) {
        console.error("[模板名]必填 - 请输入新的模板名称");
        console.log("[模板名] [页面名]");
        process.exit(1);
    }
    if (!process.argv[3]) {
        console.error("[页面名]必填 - 请输入新的页面命名");
        process.exit(1);
    }
}
// 参数初始化
function argumentInit(options={}){
    options.pathName = process.argv[2]
    options.TemplateName = process.argv[3]
    options.templateRouteJSON = require(options.templateRoutePath);
    options.modulesJSON = require(options.modulesPath);
    options.viewDir = path.join(getSrcDir(), "./" + options.view)
}
function init(options={}){
    modulesVerification(options)
    argumentInit(options)
}
module.exports = {
    init,
    modulesVerification
}