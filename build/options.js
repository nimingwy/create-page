const options = {
    // 路径
    templateRoutePath:"./template.route.json",
    templateRouteList:[],
    modulesPath:"./template.modules.json",
    modulesJSON: {},
    varPath:"./template.var.json",
    // 名称信息
    pathName:null,
    TemplateName:null,
    // 模板
    replaceTemplate:{},
    _pageList:[],
    view:"view",
    viewDir:null,
    // dir
    pageName:"",
    pagaList:[],
    // 页面内容
    pageTemplateContent:"",
    dirPath:"",
    DIR_OUTPUT_PATH_VUE:"",
    DIR_OUTPUT_PATH_LESS:"",
}

const routeOptions = {
    path: undefined,
    name: undefined,
    components: undefined,
    meta: {
        title:""
    },
};

module.exports = {
    options,
    routeOptions
}