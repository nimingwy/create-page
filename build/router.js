import Vue from 'vue'
import Router from 'vue-router'
import resources from './resources'
import productsRouter from "./products"
import systemConfig from "./systemConfig"
import hostsRouter from "./hosts"
import operationDataRouter from "./operationData"
import getPageTitle from '@/utils/get-page-title'
//插件
// import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'
Vue.use(Router)

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};


const router = [
  {
    "path": "/ggg22008756",
    "name": "ggg22008756",
    "components": "./view/ggg22008756/index.vue"
  },
  {
    "path": "/ggg220087567",
    "name": "ggg220087567",
    "components": "./view/ggg220087567/index.vue"
  },
  {
    "path": "/ggg2200875670",
    "name": "ggg2200875670",
    "components": "./view/ggg2200875670/index.vue"
  },
  {
    "path": "/ggg2200875670569",
    "name": "ggg2200875670569",
    "components": "./view/ggg2200875670569/index.vue"
  },
  {
    "path": "/ggg22008756705696757",
    "name": "ggg22008756705696757",
    "components": "./view/ggg22008756705696757/index.vue"
  },
  {
    "path": "/gg535",
    "name": "gg535",
    "components": "./view/gg535/index.vue"
  },
  {
    "path": "/gg53545",
    "name": "gg53545",
    "components": "./view/gg53545/index.vue"
  },
  {
    "path": "/gg535456",
    "name": "gg535456",
    "components": "./view/gg535456/index.vue",
    "meta": {
      "name": 1
    }
  },
  {
    "path": "/gg5354566",
    "name": "gg5354566",
    "components": "./view/gg5354566/index.vue",
    "meta": {
      "title": "gg5354566"
    }
  },
  {
    "path": "/gg535456645",
    "name": "gg535456645",
    "components": "./view/gg535456645/index.vue",
    "meta": {
      "title": "gg535456645"
    }
  },
  {
    "path": "/5gg535456645",
    "name": "5gg535456645",
    "components": "./view/5gg535456645/index.vue",
    "meta": {
      "title": "5gg535456645"
    }
  },
  {
    "path": "/gg53545664555",
    "name": "gg53545664555",
    "components": "./view/gg53545664555/index.vue",
    "meta": {
      "title": "gg53545664555"
    }
  },
  {
    "path": "/gg5354566455545",
    "name": "gg5354566455545",
    "components": "./view/gg5354566455545/index.vue",
    "meta": {
      "title": "gg5354566455545"
    }
  },
  {
    "path": "/gg53545664555455",
    "name": "gg53545664555455",
    "components": "./view/gg53545664555455/index.vue",
    "meta": {
      "title": "gg53545664555455"
    }
  }
]

const newRouter = new Router({
  routes: router
})

export default newRouter

