
const {init} = require("./init")
const {options,routeOptions} = require("./options")
init(options)

const path = require("path");
const fs = require("fs");

const {hasFile,hasFileName,hasModules,setRouteInfo,saveVue,saveFile,saveLess,saveRouterTemplate} = require("./file")


// 模板变量更换
function readTemplateContent({replaceTemplate,varPath}){
  const vartemplatePath = path.join(__dirname, varPath)
  const varTemplate = fs.existsSync(vartemplatePath)
  if(!varTemplate){
    fs.writeFileSync(vartemplatePath,JSON.stringify({}, null, {}),"utf-8")
  }else{
    const templateContent = require(vartemplatePath)
    const _keys = Object.keys(templateContent)
    _keys.forEach(key=>{
      if(!replaceTemplate[key]){
        replaceTemplate[key] = templateContent[key]
      }
    })
  }
}

// 信息验证
hasFileName(options)
hasModules(options)
hasFile(options)
// 设置保存信息
setRouteInfo(options,routeOptions)
readTemplateContent(options)
saveVue(options.DIR_OUTPUT_PATH_VUE, options.pageTemplateContent, options.replaceTemplate);
saveLess(options.DIR_OUTPUT_PATH_LESS, "",options.replaceTemplate);
saveRouterTemplate(options);
saveFile(options);
