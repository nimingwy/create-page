import Vue from 'vue'
import Router from 'vue-router'
import resources from './resources'
import productsRouter from "./products"
import systemConfig from "./systemConfig"
import hostsRouter from "./hosts"
import operationDataRouter from "./operationData"
import getPageTitle from '@/utils/get-page-title'
//插件
// import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'
Vue.use(Router)

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};


const router = {{routerList}}
const newRouter = new Router({
  routes: router
})

export default newRouter
