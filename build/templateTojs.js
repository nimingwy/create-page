const path = require("path");
const fs = require("fs");
const render = require("json-templater/string");
const fileSave = require("file-save");
const utils = require("./utils");
const root_path = utils.getSrcDir();
const route_path = path.join(__dirname, "./template.route.json");
const router_js = fs.readFileSync(path.join(__dirname, "./template.route.js"));

const route_data = fs.readFileSync(route_path)
const router = route_data.toString("utf-8")
console.log();
const out_paths = path.join(__dirname,"./router.js")
function save_route(out_path, template, replace = {}) {
  fileSave(out_path)
    .write(render(template.toString(), replace), "utf8")
    .end("\n");
}
save_route(out_paths,router_js.toString("utf-8"),{
    routerList:JSON.parse(router)
})
console.log(router_js.toString("utf-8"));
