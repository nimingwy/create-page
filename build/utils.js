const path = require("path");
// src路径
function getSrcDir() {
    return path.join(path.resolve(__dirname, ".."), "src");
  }
module.exports = {
    getSrcDir
} 