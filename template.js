modulesVerification()
const render = require("json-templater/string")
const path = require("path")
const fs = require("fs")
const fileSave = require('file-save');
const os = require("os")
const TemplateJSON = require("./template.route.json")
const modulesJSOn = require("./template.modules.json");
// 模板名称
const pathName = process.argv[2]
// 页面名称
const TemplateName = process.argv[3]
// 
const view = "view"
const viewDir = path.join(__dirname,"./"+view)
process.on('exit', () => {
    console.log();
});
// 模块验证

function modulesVerification(){
    if (!process.argv[2]) {
        console.error('[模板名]必填 - 请输入新的模板名称');
        process.exit(1);
    }
    if (!process.argv[3]) {
        console.error('[页面名]必填 - 请输入新的页面命名');
        process.exit(1);
    }
}
var IMPORT_TEMPLATE = 'import {{name}} from \'../packages/{{package}}/index.js\';';
// console.log(render(IMPORT_TEMPLATE, {
//     name: "test"
// }))

// const isAddToTemplate = TemplateJSON.some((e) => e.name===pathName)
// if(isAddToTemplate){
//     console.error('[页面名]重复 - 页面已存在,请更换其他名称');
//     process.exit(1);
// }
console.log(TemplateName);
const Modules = modulesJSOn[pathName]
if(!Modules){
    console.error('[模块]不存在 - 模块不存在,请更换其他名称');
    process.exit(1);
}
TemplateJSON.push({
    path:"/"+TemplateName,
    name:TemplateName,
    components:"./"+view+"/"+TemplateName+"/index.vue"
})
const modulesTempaltepath = path.join(__dirname,Modules)
const modulesTempalte = fs.readFileSync(modulesTempaltepath)
const modulePath = viewDir+'/'+TemplateName+'/'
const OUTPUT_PATH_VUE = modulePath+"index.vue"
const OUTPUT_PATH_LESS = modulePath+"index"+".less"
fs.mkdir("./"+view+"/"+TemplateName,function(err){
    if(err){
        console.error(err);
        process.exit(1);
    }
    console.log("页面创建成功");
})

fileSave(OUTPUT_PATH_VUE)
  .write(render(modulesTempalte,{}), 'utf8')
  .end('\n');
fs.writeFileSync(OUTPUT_PATH_LESS, "");
saveTemplate(TemplateJSON)
// console.log(TemplateJSON);
function saveTemplate(RouteArr) {
    const TemplatePath = path.join(__dirname, "./template.route.json")
    fileSave(TemplatePath).write(JSON.stringify(RouteArr,null, '  '), 'utf8').end("\n")
    console.log("路由添加成功")
}